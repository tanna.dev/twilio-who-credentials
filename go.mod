module gitlab.com/tanna.dev/twilio-who-credentials

go 1.20

require github.com/twilio/twilio-go v1.3.4

require (
	github.com/golang/mock v1.6.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
