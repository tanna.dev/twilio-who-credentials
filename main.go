package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/twilio/twilio-go"
	api "github.com/twilio/twilio-go/rest/api/v2010"
)

func main() {
	accountSid := os.Getenv("TWILIO_ACCOUNT_SID")

	authToken := os.Getenv("TWILIO_AUTH_TOKEN")

	apiKey := os.Getenv("TWILIO_API_KEY")
	apiSecret := os.Getenv("TWILIO_API_SECRET")

	apiKeyAuth := apiKey != ""

	var client *twilio.RestClient

	if authToken != "" {
		client = twilio.NewRestClientWithParams(twilio.ClientParams{
			Username: accountSid,
			Password: authToken,
		})
	}

	if apiKeyAuth {
		client = twilio.NewRestClientWithParams(twilio.ClientParams{
			Username:   apiKey,
			Password:   apiSecret,
			AccountSid: accountSid,
		})
	}

	if client == nil {
		log.Fatal("No API client could be created - ensure that you have provided the credentials to check in the environment")
	}

	// an API call that requires a `Standard` or `Main` API Key or an account's Auth Token
	canListServerlessServices := listServerlessServices(client)

	// an API call that requires a `Main` API Key or an account's Auth Token
	accountInfo, canListAccounts, accountFound := listAccounts(client, accountSid)
	if canListServerlessServices && !canListAccounts {
		if apiKeyAuth {
			fmt.Println("Token is an (active) `Standard` API Key (or this is a `Main` API key, but the AccountSid didn't match) ")
		} else {
			fmt.Println("Unsure how we've gotten here - this should only be possible using a `Standard` API Key")
			os.Exit(3)
		}
	} else if canListServerlessServices && canListAccounts {
		fmt.Println("Authenticated successfully as a `Main` type of API key")
		if accountFound {
			fmt.Printf("Account SID %s found with info:\n%s\n", accountSid, accountInfo)
		}
	} else if !canListServerlessServices && !canListAccounts {
		fmt.Println("These credentials don't _seem_ to be valid")
		os.Exit(1)
	} else {
		fmt.Print("Something else happened:\n", canListServerlessServices, canListAccounts)
		fmt.Printf("canListServerlessServices: %v\n", canListServerlessServices)
		fmt.Printf("canListAccounts: %v\n", canListAccounts)
		os.Exit(2)
	}
}

func listServerlessServices(client *twilio.RestClient) bool {
	_, err := client.ServerlessV1.ListService(nil)
	return err == nil
}

// listAccounts
// returns:
// string: the account information
// bool: whether the API call succeeded
// bool: whether the account could be found
func listAccounts(client *twilio.RestClient, accountSid string) (string, bool, bool) {
	params := &api.ListAccountParams{}
	params.SetLimit(20)

	found := false
	resp, err := client.Api.ListAccount(params)
	if err != nil {
		return "", false, false
	}

	var out strings.Builder
	for _, r := range resp {
		if r.Sid != nil && *r.Sid != accountSid {
			continue
		}

		out.WriteString(fmt.Sprintf("Found entry for Account SID %s:\n", accountSid))

		safePrint("SID", r.Sid)
		safePrint("Status", r.Status)
		safePrint("DateCreated", r.DateCreated)
		safePrint("DateUpdated", r.DateUpdated)
		safePrint("OwnerAccountSid", r.OwnerAccountSid)
		safePrint("Type", r.Type)

		found = true
	}
	return out.String(), true, found
}

func safePrint(prefix string, s *string) string {
	var out strings.Builder
	out.WriteString(fmt.Sprintf("%s: ", prefix))

	if s != nil {
		out.WriteString(fmt.Sprintf(*s))
	} else {
		out.WriteString(fmt.Sprintf("<nil>"))
	}
	out.WriteString("\n")

	return out.String()
}
