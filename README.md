# Twilio authentication checker

A command-line tool to check the validity of Twilio credentials.

See [_Who do these Twilio credentials belong to?_](https://www.jvt.me/posts/2023/03/08/twilio-who-credentials/) for more details.

## Usage

First, install it:

```sh
go install gitlab.com/tanna.dev/twilio-who-credentials@HEAD
```

Note that in all cases, the `TWILIO_ACCOUNT_SID` needs to correctly match the (sub)account that the credentials are for.

### With an Auth Token

An auth token may be found in the Twilio console for a given Account SID.

```sh
env TWILIO_ACCOUNT_SID=... \
  TWILIO_AUTH_TOKEN=... \
  twilio-who-credentials
```

The command will return a non-zero status code if the auth token could be used. If valid, information about the account will be printed out, such as the account name.

### With an API Key

An API key may be issued from the Twilio console, and may be either a `Main` or a `Standard` token. This tool detects, as best as it can, the type of token in use.

```sh
env TWILIO_ACCOUNT_SID=... \
  TWILIO_API_KEY=... \
  TWILIO_API_SECRET=... \
  twilio-who-credentials
```

The command will return a non-zero status code if the API key could be used. If valid, information will be returned as to whether it's a `Main` or `Standard` API key.

If a `Main` API key, information about the account will be printed out, such as the account name.

### Specifying the region

This library uses [the configuration available in Twilio's Go SDK](https://github.com/twilio/twilio-go#specify-a-region-andor-edge) to specify the region and edge location, so can be configured as such:

```sh
env TWILIO_ACCOUNT_SID=... \
  TWILIO_API_KEY=... \
  TWILIO_API_SECRET=... \
  TWILIO_REGION=au1 \
  TWILIO_EDGE=sydney \
  twilio-who-credentials
```

## License

This code is licensed under Apache-2.0
